
local PALETTE_DB = require 'database.palette'
local State = require 'state'
local GameOver = require 'view.gameover'
local Vec = require 'common.vec'

local GameOverState = require 'common.class' (State)

function GameOverState:_init(stack)
    self:super(stack)
end

function GameOverState:enter(message)
    local w, h = love.graphics.getDimensions()
    self.message = GameOver(message)
    local size = self.message.size
    self.message.position = Vec(w / 2 - size.x / 2, h / 2)

    love.graphics.setBackgroundColor(PALETTE_DB.black)
    self:view('hud'):add('gameover_message', self.message)
end

function GameOverState:suspend()
    self:view('hud'):remove('gameover_message')
end

function GameOverState:leave()
    self:view('hud'):remove('gameover_message')
    self:view('hud'):remove(self.message)
end

function GameOverState:on_keypressed(key)
    if key == 'return' then
        return self:pop()
    end
end

return GameOverState


