local Wave = require 'model.wave'
local Unit = require 'model.unit'
local Vec = require 'common.vec'
local Cursor = require 'view.cursor'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local Stats = require 'view.stats'
local State = require 'state'
local StoreView = require 'view.store_view'
local Store = require 'model.store'

local PlayStageState = require 'common.class' (State)

function PlayStageState:_init(stack)
    self:super(stack)
    self.stage = nil
    self.cursor_bf = nil
    self.cursor_store = nil
    self.atlas = nil
    self.battlefield = nil
    self.capital = nil
    self.units = nil
    self.wave = nil
    self.waves = nil
    self.waveid = nil
    self.stats = nil
    self.monsters = nil
    self.monsters_count = 0
    self.store = nil
    self.store_view = nil
    self.store_units = nil
end

function PlayStageState:enter(params)
    self.stage = params.stage
    self.store = Store(params.stage)
    self:_load_view()
    self:_load_units()
end

function PlayStageState:suspend()
    self:view('bg'):remove('battlefield')
    self:view('fg'):remove('atlas')
    self:view('bg'):remove('cursor')
    self:view('hud'):remove('stats')
    self:view('hud'):remove('store')
    self:view('hud'):remove('cursor')
end

function PlayStageState:resume()
    return self:pop()
end

function PlayStageState:leave()
    self:view('bg'):remove('battlefield')
    self:view('fg'):remove('atlas')
    self:view('bg'):remove('cursor')
    self:view('hud'):remove('stats')
    self:view('hud'):remove('store')
    self:view('hud'):remove('cursor')
end

function PlayStageState:_load_view()
    self.battlefield = BattleField()
    self.atlas = SpriteAtlas()
    self.cursor_bf = Cursor(self.battlefield)
    local _, right, top, _ = self.battlefield.bounds:get()
    local halfheight = self.battlefield.size.y
    self.stats = Stats(Vec(right + 32, top))
    self.store_view = StoreView(
        self.store, Vec(right, top + 16 + 3 * halfheight / 2)
    )
    self.cursor_store = Cursor(self.store_view)
    self:view('bg'):add('battlefield', self.battlefield)
    self:view('fg'):add('atlas', self.atlas)
    self:view('bg'):add('cursor', self.cursor_bf)
    self:view('hud'):add('stats', self.stats)
    self:view('hud'):add('store', self.store_view)
    self:view('hud'):add('cursor', self.cursor_store)

    self.store_units = {}
    local pos = self.store_view.center - Vec(32, 0)
    for _, unit in pairs(self.store.units) do
        local u = self:_create_unit_at(unit.specname, pos, false)
        self.store_units[u] = true
        pos = pos + Vec(32, 0)
    end
end

function PlayStageState:_load_units()
    local pos = self.battlefield:tile_to_screen(-6, 6)
    self.units = {}
    self.capital = self:_create_unit_at('capital', pos, true)
    self.waves = self.stage.waves
    self.waveid = 1
    self.wave = Wave(self.waves[self.waveid])
    self.wave:start()
    self.monsters = {}
end

function PlayStageState:_create_unit_at(specname, pos, hpbar)
    local unit = Unit(specname)
    self.atlas:add(unit, pos, unit:get_appearance(), hpbar)
    return unit
end

function PlayStageState:_kill_monster(specname)
    self.atlas:remove(specname)
    self.monsters[specname] = nil
    self.monsters_count = self.monsters_count - 1
end

function PlayStageState:on_mousepressed(x, y, button)
    if button == 1 then
        local bfbounds = self.battlefield.bounds
        local stbounds = self.store_view.bounds

        if x >= bfbounds.left and x <= bfbounds.right and y >= bfbounds.top and
            y <= bfbounds.bottom then
            if self.store.money >= self.store.selected.cost then
                local unit = self:_create_unit_at(
                    self.store.selected.specname, Vec(self.cursor_bf:get_position()), true
                )
                self.units[unit] = true
                self.store.money = self.store.money - self.store.selected.cost
            end
        elseif x >= stbounds.left and x <= stbounds.right and
            y >= stbounds.top and y <= stbounds.bottom then
            local pos = self.store_view:get_unit_pos(
                Vec(self.cursor_store:get_position())
            )
            self.store.selected = self.store.units[pos]
        end
    end
end

function PlayStageState:show_unit_stats(units_list, x, y)
    for unit in pairs(units_list) do
        local unit_sprite_instance = self.atlas:get(unit)
        local unit_pos = unit_sprite_instance.position
        local unit_size = self.atlas.get_framesize()
        local cursor_pos = Vec(x, y)

        if cursor_pos <= unit_pos + unit_size / 2 and
           cursor_pos >= unit_pos - unit_size / 2 then
            self.stats.unit_name = unit.spec.name
            self.stats.unit_range = "Range: " .. unit:get_range()
            self.stats.unit_damage = "Damage: " .. unit:get_damage()
            self.stats.unit_speed = "Speed: " .. unit:get_speed()
            self.stats.unit_cost = "Cost: " .. unit:get_cost()
       end
    end
end

function PlayStageState:on_mousemoved(x, y)
    self:show_unit_stats(self.monsters, x, y)
    self:show_unit_stats(self.units, x, y)
    self:show_unit_stats(self.store_units, x, y)
end

function PlayStageState:update(dt)
    local capital_pos = self.atlas:get(self.capital).position

    if self.wave.count == 0 and self.monsters_count == 0 then
        self.waveid = self.waveid + 1

        if self.waveid > #self.waves then
            love.timer.sleep(3)
            return self:push(
                'gameover_stage',
                "Congratulations, you win! Press RETURN to quit."
            )
        end

        self.stats.wave_info = ""
        self.wave.left = self.wave.delay + 2
        self.wave = Wave(self.waves[self.waveid])
        self.wave:start()
        self.monsters_count = 0
    end

    self.wave:update(dt)

    local pending = self.wave:poll()
    local rand = love.math.random

    while pending > 0 and self.wave.count > 0 do
        self.stats.wave_info = "Wave " .. self.waveid .. " start!"
        local x, y = rand(5, 7), -rand(5, 7)
        local pos = self.battlefield:tile_to_screen(x, y)
        local next = self.wave:next()
        local monster = self:_create_unit_at(next, pos, true)
        self.monsters[monster] = true
        self.monsters_count = self.monsters_count + 1
        pending = pending - 1
    end

    for monster in pairs(self.monsters) do
        local monster_sprite_instance = self.atlas:get(monster)
        local monster_pos = monster_sprite_instance.position
        local min_dist = capital_pos:distance(monster_pos)
        local min_mov

        for i = -1, 1, 1 do
            for j = -1, 1, 1 do
                local mov = Vec(i, j)
                local dist = capital_pos:distance(monster_pos + mov)

                if dist <= min_dist then
                    min_mov = mov
                    min_dist = dist
                end
            end
        end

        monster_sprite_instance.position:add(min_mov * monster:get_speed() * dt)

        for unit in pairs(self.units) do
            local unit_pos = self.atlas:get(unit).position
            local dist = unit_pos:distance(monster_pos)

            if (dist <= unit:get_range()) then
                unit.sound:play()
                monster.hp = monster.hp - unit:get_damage()
            end
        end

        if min_dist < 1 or monster.hp <= 0 then
            self:_kill_monster(monster)
        elseif min_dist < monster:get_range() then
            monster.sound:play()
            self.capital.hp = self.capital.hp - monster:get_damage()

            if self.capital.hp <= 0 then
                return self:push(
                    'gameover_stage',
                    "You failed! Press RETURN to quit."
                )
            end
        end
    end
end

return PlayStageState

