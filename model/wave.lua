
local Wave = require 'common.class' ()

local function shuffle(t)
    local rand = math.random
    assert(t, "table.shuffle() expected a table, got nil")
    local iterations = #t
    local j

    for i = iterations, 2, -1 do
        j = rand(i)
        t[i], t[j] = t[j], t[i]
    end
end

function Wave:_init(spawns)
    self.spawns = {}

    for name, count in pairs(spawns) do
        for _ = 1, count do
            table.insert(self.spawns, name)
        end
    end

    shuffle(self.spawns)
    self.delay = 3
    self.count = #self.spawns
    self.left = nil
    self.pending = 0
end

function Wave:start()
    self.left = self.delay
end

function Wave:update(dt)
    self.left = self.left - dt
    if self.left <= 0 then
        self.left = self.left + self.delay
        self.pending = self.pending + 1
    end
end

function Wave:poll()
    local pending = self.pending
    self.pending = 0
    return pending
end

function Wave:next()
    self.count = self.count - 1
    return table.remove(self.spawns, 1)
end

return Wave

