
local Store = require 'common.class' ()

function Store:_init(stage)
    self.money = stage.money
    self.units = {}

    for i, unit in pairs(stage.units) do
        local u = require ('database.units.' .. unit.name)
        u.specname = unit.name
        self.units[i] = u
    end

    self.selected = self.units[1]
end

return Store

