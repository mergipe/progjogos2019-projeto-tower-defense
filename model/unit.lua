
local Unit = require 'common.class' ()

function Unit:_init(specname)
    local spec = require('database.units.' .. specname)
    self.spec = spec
    self.hp = spec.max_hp
    self.range = spec.range
    self.damage = spec.damage
    self.speed = spec.speed
    self.cost = spec.cost

    if not self.cost then self.cost = 0 end
    if not self.speed then self.speed = 0 end

    if spec.sound then
        self.sound = love.audio.newSource('assets/audio/' .. spec.sound, 'static')
        self.sound:setVolume(0.4)
    end
end

function Unit:get_name()
    return self.spec.name
end

function Unit:get_appearance()
    return self.spec.appearance
end

function Unit:get_hp()
    return self.hp, self.spec.max_hp
end

function Unit:get_range()
    return self.range
end

function Unit:get_damage()
    return self.damage
end

function Unit:get_speed()
    return self.speed
end

function Unit:get_cost()
    return self.cost
end

return Unit

