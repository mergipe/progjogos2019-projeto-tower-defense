
return {
    name = "Priest",
    max_hp = 3,
    appearance = 'priest',
    range = 75,
    damage = 0.03,
    sound = 'Bow1.ogg',
    cost = 250
}

