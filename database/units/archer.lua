
return {
    name = "Archer",
    max_hp = 5,
    appearance = 'archer',
    range = 100,
    damage = 0.05,
    sound = 'Bow1.ogg',
    cost = 200
}

