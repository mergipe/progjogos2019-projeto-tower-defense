
return {
    name = "Blue Slime",
    max_hp = 10,
    appearance = 'blue_slime',
    range = 20,
    damage = 2,
    speed = 30,
    sound = 'Slime.ogg'
}

