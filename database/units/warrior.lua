
return {
    name = "Warrior Troop",
    max_hp = 10,
    appearance = 'knight',
    range = 50,
    damage = 0.1,
    sound = 'Blow3.ogg',
    cost = 100
}

