
return {
    name = "Green Slime",
    max_hp = 4,
    appearance = 'slime',
    range = 10,
    damage = 1,
    speed = 20,
    sound = 'Slime.ogg'
}

