
return {
    name = "Chicken",
    max_hp = 30,
    appearance = 'chicken',
    range = 15,
    damage = 3,
    speed = 25,
    sound = 'Slash10.ogg'
}

