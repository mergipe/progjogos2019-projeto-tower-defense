
return {
    title = 'Slime Infestation',
    money = 1000,
    waves = {
        { green_slime = 1 },
        { green_slime = 1 },
        { blue_slime = 1, green_slime = 1, chicken = 1 },
        { chicken = 1 }
    },
    units = {
        { name = 'warrior' },
        { name = 'archer' },
        { name = 'priest' }
    }
}

