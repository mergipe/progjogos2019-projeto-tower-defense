
local Stats = require 'common.class' ()

function Stats:_init(position)
    self.position = position
    self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
    self.font:setFilter('nearest', 'nearest')
    self.unit_name = ""
    self.unit_range = ""
    self.unit_damage = ""
    self.unit_speed = ""
    self.unit_cost = ""
    self.wave_info = ""
end

function Stats:draw()
    local g = love.graphics
    g.push()
    g.setFont(self.font)
    g.setColor(1, 1, 1)
    g.translate(self.position:get())
    g.print(("%s\n"):format(self.wave_info))
    g.print(("\n\n\n%s\n%s\n%s\n%s\n%s"):format(
        self.unit_name, self.unit_range, self.unit_damage,
        self.unit_speed, self.unit_cost
    ))
    g.translate(0, self.font:getHeight())
    g.pop()
end

return Stats

