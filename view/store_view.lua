
local Box = require 'common.box'
local Vec = require 'common.vec'

local StoreView = require 'common.class' ()

function StoreView:_init(store, position)
    self.store = store
    self.position = position
    self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
    self.font:setFilter('nearest', 'nearest')
    local w2 = 32 * 2
    local h2 = 32 * 1
    self.center = position + Vec(32 * 3, 48 - 4 + self.font:getHeight())
    self.size = Vec(w2, h2)
    self.bounds = Box.from_vec(self.center, self.size)
end

function StoreView:round_to_tile(pos)
    local dist = pos - self.center
    local tile = ((dist + Vec(16, 16)) / 32):floor()
    tile:clamp(1, 0)
    return self.center + tile * 32
end

function StoreView:get_unit_pos(pos)
    if pos == self.center then
        return 2
    elseif pos == self.center - Vec(32, 0) then
        return 1
    elseif pos == self.center + Vec(32, 0) then
        return 3
    end
end

function StoreView:draw()
    local g = love.graphics
    g.push()
    g.setFont(self.font)
    g.setColor(1, 1, 1)

    g.translate(self.position.x + 32, self.position.y)
    g.print("Store ($" .. self.store.money .. ")")

    g.pop()
    g.setColor(1, 1, 1)
    g.setLineWidth(4)
    g.rectangle('line', self.bounds:get_rectangle())
end

return StoreView

