
local Vec = require 'common.vec'

local GameOver = require 'common.class' ()

function GameOver:_init(message)
    self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
    self.font:setFilter('nearest', 'nearest')
    self.position = Vec()
    self.message = love.graphics.newText(
        self.font, message
    )
    local width, height = self.message:getDimensions()
    self.size = Vec(width, height)
end

function GameOver:draw()
    local g = love.graphics
    g.push()
    g.translate(self.position:get())
    g.setColor(1, 1, 1)
    g.setLineWidth(4)

    g.setColor(1, 1, 1)
    g.draw(self.message, 0, 0)
    g.pop()
end

return GameOver

